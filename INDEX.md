# RECOVER

Recover disks or lost files.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## RECOVER.LSM

<table>
<tr><td>title</td><td>RECOVER</td></tr>
<tr><td>version</td><td>0.1 (BETA) (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2004-02-07</td></tr>
<tr><td>description</td><td>Recover disks or lost files.</td></tr>
<tr><td>keywords</td><td>recover, disks, files</td></tr>
<tr><td>author</td><td>Imre Leber &lt;imre.leber -AT- telenet.be&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Imre Leber &lt;imre.leber -AT- telenet.be&gt;</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Recover</td></tr>
</table>
